""" tm_parser module """

from .parser import Parser, dump_string

__all__ = [
    "Parser",
    "dump_string",
]
